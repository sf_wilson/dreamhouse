/*************************************************************************************************************
 * @name			MyClass
 * @author			sf-wilson <itsme_web@sina.com>
 * @created			13 / 07 / 2020
 * @description		Description of your code
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2020-07-13		sf-wilson				Changes desription
 *
**************************************************************************************************************/
public with sharing class MyClass {
    public MyClass() {
        sayHello('Wilson');
        sayHello('Trailblazer');
        sayHello('CI/CD Pipeline');
    }

    /*********************************************************************************************************
     * @name			sayHello
     * @author			sf-wilson <itsme_web@sina.com>
     * @created			13 / 07 / 2020
     * @description		Description of your code
     * @param			String name : the name you want to say hello to
     * @return			void
    **********************************************************************************************************/
    public static void sayHello(String name) {
        System.debug('Hello, ' + name + '!');
    }
}
